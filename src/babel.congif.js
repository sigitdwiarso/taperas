module.exports = function (api) {
    
  const presets = ["next/babel"];
  const plugins = [
    "@babel/plugin-proposal-do-expressions"
  ];   /*change this value when you have a lot of tests*/
 api.cache(false);    return {
      presets,
      plugins
  }};