#!/bin/sh

set -e

apk add --update docker python make g++
rm -rf build node-modules

cat > default-nginx.conf <<EOF
server {
    listen       8080;
    server_name  localhost;
    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
        try_files \$uri /index.html;
    }
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
EOF

yarn config set cache-folder /yarn

if [ -n "$HTTP_PROXY" ];then
  yarn config set proxy $HTTP_PROXY
  yarn config set https-proxy $HTTP_PROXY
fi

export REACT_APP_IMAGE_TAG=$IMAGE_TAG
export REACT_APP_BUILD_ID=$BUILD_ID
export SASS_PATH=./node_modules 
yarn install
yarn build


if [ "x" != "x$RUNNER_USER" ];then
    chown -R $RUNNER_USER:$RUNNER_USER .
fi