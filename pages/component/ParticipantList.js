import React from 'react';
import MaterialTable from 'material-table';
import Title from './Title';
import { forwardRef } from 'react';
import fetch from 'node-fetch';
import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import Visibility from '@material-ui/icons/Visibility';
import Router from 'next/router'

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Visibility: forwardRef((props, ref) => <Visibility {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

export default function ListPeserta(props) {
    const { value } = props
    const [state, setState] = React.useState({
        columns: [
            {
                title: 'Nomor Identitas Peserta',
                field: 'ID'
            },
            {
                title: 'Status Peserta',
                field: 'participantStatus'
            },
            {
                title: 'Status Pendaftaran',
                field: 'registrationStatus'
            },
            {
                title: 'Nama',
                field: 'name'
            },
            {
                title: 'Tanggal Lahir',
                field: 'dateOfBirth'
            },
            {
                title: 'Nama Pemberi Kerja',
                field: 'employerName'
            },
        ],
        data: value.participant !== null ? value.participant : []
    });
    
    return (
        <MaterialTable
            title="Daftar Peserta"
            icons={tableIcons}
            columns={state.columns}
            data={value.listParticipant.participant}
            actions={[
                {
                  icon: () => <Visibility />,
                  tooltip: 'Detail',
                  onClick: (event, rowData) => {
                    let page = "/Detail/"+rowData.ID
                    Router.push(page)
                  }
                }
            ]}
            options={{
                actionsColumnIndex: -1
              }}
        />
    );
}