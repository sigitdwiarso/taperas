import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PeopleIcon from '@material-ui/icons/People';
import Link from 'next/link'

export default function mainListItems(){
  return(
    <div>
    <Link href="/">
      <ListItem button>
        <ListItemIcon>
          <PeopleIcon />
        </ListItemIcon>
        <ListItemText primary="Pengelolaan Peserta" />
      </ListItem>
    </Link>
  </div>
  )
}

