import PaticipantList from './component/ParticipantList';

export default function Home(props){
  const {listParticipant} = props
  return(
    <PaticipantList value = {props}/>
  )
}

export async function getServerSideProps() {
  var payload = {
    "omama": "olala"
  }
  const res = await fetch("http://localhost:8000/api/tapera_participant/ParticipantService/GetParticipants",{
    method: 'POST',
    body: JSON.stringify(payload),
    headers: {'Content-Type': 'application/json'}
  })

  const listParticipant = await res.json()
  return {
    props:{
      listParticipant
    }
  }

}