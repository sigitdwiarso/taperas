import Box from '@material-ui/core/Box'
import Divider from '@material-ui/core/Divider'
import Typography from '@material-ui/core/Typography'

function DataFinansial(props) {
  const { value } = props
    return (
        <Box display="flex" p={2} flexDirection="column">
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Nomor Rekering Taburgan
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {value.data.bank_item[0].account_number}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Nama Pemrlik Rekening
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                    {value.data.bank_item[0].name}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Nama Bank/Cabang Bank
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                      {value.data.bank_item[0].bank_name}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Gaji Pokok
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                    {value.data.items.salary}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Jenis Tunjangan Keluarga
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        Tunjangan Istri/Suami dan 2 Orang Anak {/* not set on database */}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Persentasi Tunjangan Keluarga
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        0,14 {/* not set on database */}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Nilai Tunjangan Keluarga
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {value.data.items.balance}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Unit Penyertaan
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {value.data.items.inclusion_unit}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Saldo Simpanan
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        Rp372.602,00 {/* not set on database */}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Jenis Piiihan Investasi
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {value.data.investation === 2 ? "Syariah" : "Konvensional"}
                    </Typography>
                </Box>
            </Box>


        </Box>
    )
}

export default DataFinansial