import Box from '@material-ui/core/Box'
import Divider from '@material-ui/core/Divider'
import Typography from '@material-ui/core/Typography'

function DataManfaat(props) {
  const { value } = props
    return (
        <Box display="flex" p={2} flexDirection="column">
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                    Jenis Manfaat
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                    BANTUAN UANG MUKA {/* not set on database */}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                    Jumlah Manfaat
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                    Rpl.800.000,00 {/* not set on database */}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                    Bank Penyalur
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        BTN {/* not set on database */}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                    Tanggal Realisasi Manfaat
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                    31 Desember 2020 {/* not set on database */}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                    Informasi Kepemilikan Rumah
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {value.data.info.own_home === 1 ? " Memiliki Rumah" : "Belum Memiliki Rumah" }
                    </Typography>
                </Box>
            </Box>

        </Box>
    )
}

export default DataManfaat