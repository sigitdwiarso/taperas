import Box from '@material-ui/core/Box'
import Divider from '@material-ui/core/Divider'
import Typography from '@material-ui/core/Typography'

function DataPekerjaan(props) {
  const{ value } = props
  let startTime = new Date(value.data.items.start_time)
  let retirementTime = new Date(value.data.items.retirement_time)
    return (
        <Box display="flex" p={2} flexDirection="column">
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                    Nomor Identitas Pekerja
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                      {value.data.items.identity_work_number}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                    Nama Instilus
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                      {value.data.items.institution_name}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                    Pangkat/Jabatan
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {value.data.items.department}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                    Status Pekerja
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                      {value.data.items.status === 1 ? "Aktif" : "Tidak Aktif"}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                    Tanggal Mulai Bekerja 
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                      {startTime.toString()}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                    Tanggal Pensiun
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                      {retirementTime.toString()}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                    Batas Usia Pensiun
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                      {value.data.items.retirement_age}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                    Masa Kerja Golongan/Jabatan
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                    10 years 11 months 6 days {/* not yet set on database*/}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                    Total Masa Kerja
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                    32 years 6 months 6 days {/* not yet set on database*/}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                    Sisa Masa Kerja
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                    5 years 10 months 18 days {/* not yet set on database*/}
                    </Typography>
                </Box>
            </Box>
            

        </Box>
    )
}

export default DataPekerjaan