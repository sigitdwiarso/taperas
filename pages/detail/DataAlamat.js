import React, {useState} from 'react';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import fetch from 'node-fetch';

function DataAlamat(props) {
  const { value } = props
  return (
    <div>
      {value.data.address.map((item, index) => (
        <Box display="flex" p={2} flexDirection="column">
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Jenis Alamat
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        Tempat Tinggal {/* not set on database */}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Alamat
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {item.address}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Nomor RT
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {item.rt_number}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Nomor RW
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {item.rw_number}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Kelurahan
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {item.village}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Kecamatan
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                      {item.sub_distinct}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Kabupaten/Kotamadya
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {item.city}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Kode Pos
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {item.postal_code}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Provinsi
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {item.province}
                    </Typography>
                </Box>
            </Box>
            <Divider />
            <Box display="flex" flexDirection="row" flex={1} p={1}>
                <Box width="30%">
                    <Typography>
                        Negara
                    </Typography>
                </Box>
                <Box>
                    <Typography>
                        {item.contry}
                    </Typography>
                </Box>
            </Box>
        </Box>
      ))}
    </div>
  )
}

export default DataAlamat