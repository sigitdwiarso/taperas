import AxiosService from './service-axios';
import ServiceMock from './service-mock';


const RestClient = process.env.NODE_ENV !== 'test' 
    ? ServiceMock
    : AxiosService;

export default RestClient;